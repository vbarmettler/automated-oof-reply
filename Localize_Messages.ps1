<#
.SYNOPSIS
    Extracts file content and replaces placeholders with culture-specific variables.

.DESCRIPTION
    This script extracts the content of files and replaces placeholders with culture-specific variables. It is designed to be used in conjunction with the Automated_OOF.ps1 script for automated out-of-office replies.

.PARAMETER oofEnd
    Specifies the end date for the out-of-office reply and will be shown in the out-of-office reply. Defaults to the current date plus one day.

.PARAMETER cultures
    Specifies the cultures for which the placeholders should be replaced. Defaults to "en-US" and "de-CH".

.PARAMETER messageInternFile
    Mandatory parameter that specifies the path to the file containing the internal message template. It must contain the placeholder {returnDateEmail_en-US} and {returnDateEmail_de-CH}.

.PARAMETER messageExternFile
    Mandatory parameter that specifies the path to the file containing the external message template. It must contain the placeholder {returnDateEmail_en-US} and {returnDateEmail_de-CH}.

.NOTES
    - The variables specified by the messageInternFile and messageExternFile parameters must be in the format {returnDateEmail_en-US} and {returnDateEmail_de-CH}.
    - oofEnd, messageIntern and messageExtern can be used as pipeline input for Automated_OOF.ps1

.EXAMPLE
    Localize_Messages.ps1 -oofEnd (Get-Date).AddDays(7) -messageInternFile "C:\Templates\messageIntern.html" -messageExternFile "C:\Templates\messageExtern.html"
    Extracts the content of the specified files and replaces the placeholders with culture-specific variables. The out-of-office reply will be active for 7 days.
#>

param(
    [Alias("end")]
    $oofEnd = (Get-Date -Hour "00" -Minute "00" -Second "00").AddDays(1),

    [Alias("c")]
    [array]$cultures = @("en-UK", "de-CH"),

    [Alias("pP")]
    [string]$placeholderPrefix = "returnDateEmail_",

    [Alias("dF")]
    [array]$dataFormats = @("dddd, dd. MMMM yyyy"),

    [Alias("mIn", "in")]
    [Parameter(Mandatory)]
    [System.IO.FileInfo]$messageInternFile,

    [Alias("mEx", "ex")]
    [Parameter(Mandatory)]
    [System.IO.FileInfo]$messageExternFile
)

# Check if return date is a weekend and if that's the case set it to Monday
if ($oofEnd.DayOfWeek -eq "Saturday") {
    $oofEnd = $oofEnd.AddDays(2)
} elseif ($oofEnd.DayOfWeek -eq "Sunday") {
    $oofEnd = $oofEnd.AddDays(1)
}

# Initialize hashtable
$cultureVariables = @{}

# Iterate through the cultures to create placeholders
foreach ($culture in $cultures) {
    foreach ($dataFormat in $dataFormats) {
        # Create a placeholder suffix where the index of the data format is appended
        $placeholderSuffix = "_" + [Array]::IndexOf($dataFormats, $dataFormat)
        
        # Create a new CultureInfo object for the culture
        $cultureInfo = [System.Globalization.CultureInfo]::new($culture)

        # Create a new variable for the culture
        $cultureVariable = $placeholderPrefix + $culture + $placeholderSuffix

        # Update the hashtable
        $cultureVariables[$cultureVariable] = $oofEnd.ToString($dataFormat, $cultureInfo)
    }      
}

# Read the content of the message file into the message template variable
$messageInternTemplate = Get-Content -Path $messageInternFile.FullName -Raw
$messageExternTemplate = Get-Content -Path $messageExternFile.FullName -Raw

# Replace the placeholder in the message with the actual culture variable
foreach ($cultureVariable in $cultureVariables.Keys) {
    $messageInternTemplate = $messageInternTemplate -replace "{$cultureVariable}", $cultureVariables[$cultureVariable]
    $messageExternTemplate = $messageExternTemplate -replace "{$cultureVariable}", $cultureVariables[$cultureVariable]
}

# Prepare for pipeline output
$messageObject = New-Object PSObject -Property @{
    messageIntern = $messageInternTemplate
    messageExtern = $messageExternTemplate
    oofEnd = $oofEnd
}

Write-Output $messageObject