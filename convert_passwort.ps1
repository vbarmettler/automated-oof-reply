# Convert the password to a secure string
$securePassword = ConvertTo-SecureString $password -AsPlainText -Force

# Convert the secure string to an encrypted standard string
$encryptedPassword = ConvertFrom-SecureString $securePassword

# Write the encrypted password to a file
$encryptedPassword | Out-File -FilePath "password.txt"