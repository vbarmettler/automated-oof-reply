<#
.SYNOPSIS
This script automates the Out of Office (OOF) reply settings for a specified email address using the Exchange Web Services (EWS) module.

.DESCRIPTION
The script sets the OOF reply settings for a specified email address. It requires the EWS module (specifically the Microsoft.Exchange.WebServices.dll).

.PARAMETER email
Mandatory parameter that specifies the email address for which the OOF reply settings will be set.

.PARAMETER password
Mandatory parameter that specifies the password for the email address. It has to be secure string. If the password is not specified, the script will prompt for it.

.PARAMETER messageIntern
Mandatory parameter that specifies the message for internal recipients. It can be piped to the script.

.PARAMETER messageExtern
Mandatory parameter that specifies the message for external recipients. It can be piped to the script.

.PARAMETER oofStart
The start date for the OOF reply settings. If not set, the current date will be used.

.PARAMETER oofEnd
The end date for the OOF reply settings. If not set, the default date will be used. It can be piped to the script.

.PARAMETER dllPath
The file path to the Microsoft.Exchange.WebServices.dll assembly. If not set, the script will look for the assembly in the current user's folder.

.PARAMETER enableNotification
If set, the script will send email notifications in case of success or failure. If not set, the script will not send email notifications.

.PARAMETER notificationToEmail
The email address to which notifications will be sent. If not set, the email parameter will be used.

.PARAMETER notificationPassword
The password for the email address to which notifications will be sent. If not set, the password parameter will be used.

.PARAMETER SMTPServer
The SMTP server to use for sending email notifications. If not set, the script will use the default SMTP server "mail.ethz.ch".

.PARAMETER SMTPPort
The SMTP port to use for sending email notifications. If not set, the default port 587 will be used.

.PARAMETER enableLogging
If set, the script will log the OOF reply settings to a log file specified by the logFilePath parameter. If not set, the script will not log the OOF reply settings.

.PARAMETER logFilePath
The file path to the log file. If not set, the log file will be created in the Documents folder of the current user.

.NOTES
- This script requires the EWS module and the Microsoft.Exchange.WebServices.dll assembly.
- The default OOF reply settings are configured for the current date from 00:00:00 to 23:59:00.
- If the return date falls on a weekend, it is automatically adjusted to the following Monday.
- Logging is performed to a log file specified by the $logFilePath variable. If not set, logging will be skipped.
- Email notifications are sent in case of success and failures, using the SMTP server and port specified by the $SMTPServer and $SMTPPort variables. If not set, email notifications will be skipped.

.EXAMPLE
.\Automated_OOF.ps1 -email "vbarmettler@ethz.ch" -p  (Get-Content "C:\Users\OOF\Documents\ann.txt" | Convert-ToSecureString) -messageIntern "hello world!" -messageExtern "hello world!"

This example sets the OOF reply settings with the help of a password file where the password is stored with the help of "$password = Read-Host -AsSecureString -Force" and "ConvertFrom-SecureString $password | Out-File "C:\Users\OOF\Documents\ann.txt"".

.EXAMPLE
.\Automated_OOF.ps1 -em "vbarmettler@ethz.ch" -p ("password" | ConvertTo-SecureString -AsPlainText -Force) -mIn "hello world!" -mEx "hello world!"

This example sets the OOF reply settings with the help of a password string that is converted to a secure string.

.EXAMPLE
.\Automated_OOF_Scripting.ps1 -em vbarmettler@ethz.ch -p (Get-Content C:\Users\OOF\Documents\ann.txt | ConvertTo-SecureString) -l -n

This example sets the OOF reply settings with the help of a password file and aliases. It also enables logging and email notifications.
#>

param (
    [Parameter(Mandatory=$true)]
    [Alias("em")]    
    [ValidateNotNullOrEmpty()]
    [string]$email,

    [Alias("p", "pass")]
    [ValidateNotNullOrEmpty()]
    [securestring]$password,

    [Parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
    [Alias("mIn", "in")]
    [string]$messageIntern,

    [Parameter(Mandatory=$true, ValueFromPipelineByPropertyName=$true)]
    [Alias("mEx", "ex")]
    [string]$messageExtern,

    [Alias("start")]
    [datetime]$oofStart = (Get-Date),

    [Parameter(ValueFromPipelineByPropertyName=$true)]
    [Alias("end")]
    [datetime]$oofEnd = (Get-Date -Hour "00" -Minute "00" -Second "00").AddDays(1),

    [Alias("dll")]
    [string]$dllPath = (Join-Path $env:UserProfile -ChildPath "Microsoft.Exchange.WebServices.dll"),

    [Alias("notify", "n")]
    [switch]$enableNotification,

    [Alias("notifyTo", "nT")]
    [string]$notificationToEmail = $email,

    [Alias("notifyFrom", "nF")]
    [string]$notificationFromEmail = $email,

    [Alias("notifyPass", "nPa")]
    [ValidateNotNullOrEmpty()]
    [securestring]$notificationPassword = $password,

    [Alias("notifyServer", "nS")]
    [ValidateNotNullOrEmpty()]
    [string]$SMTPServer = "mail.ethz.ch",

    [Alias("notifyPort", "nPo")]
    [string]$SMTPPort = "587",

    [Alias("log", "l")]
    [switch]$enableLogging,

    [Alias("logPath", "lP")]
    [string]$logFilePath = (Join-Path $env:UserProfile -ChildPath "Documents\OOF_log.txt")
)

# If no password is set, then the user will be prompted for it
if (-not $password) {
    $password = Read-Host -AsSecureString -Prompt "Enter password for $email"
}

# Check if return date is a weekend and if that's the case set it to Monday
if ($oofEnd.DayOfWeek -eq "Saturday") {
    $oofEnd = $oofEnd.AddDays(2)
} elseif ($oofEnd.DayOfWeek -eq "Sunday") {
    $oofEnd = $oofEnd.AddDays(1)
}

# Dates for OOF-Replies
$dateStart = $oofStart.ToString("yyyy-MM-ddTHH:mm:ss")
$dateEnd = $oofEnd.ToString("yyyy-MM-ddTHH:mm:ss")

# Body for success email notification
$emailMessage = @"
<!DOCTYPE html>
<html>
<body>
<h1>Automated OOF-Reply has been successfully set</h1>
<p>The OOF-Reply has been successfully set for $email.</p>
<br>
<p><strong>From</strong>: $dateStart</p>
<p><strong>To</strong>: $dateEnd</p>
<br>
<h2>Internal Message</h2> 
<p>$messageIntern</p>
<br>
<h2>External Message</h2>
<p>$messageExtern</p>
</body>
</html>
"@

# Log function
function Log($message) {
    $timestamp = (Get-Date).ToString("dddd, dd.MM.yyyy HH:mm:ss", $cultureEnglish)
    $logMessage = "$timestamp - $message"
    $logMessage | Out-File -FilePath $logFilePath -Append
    Write-Output $logMessage
}

# Email notification function
function SendEmailNotification {
    param (
        [Parameter(Mandatory)]
        [string]$subject,

        [Parameter(Mandatory)]
        [string]$emailMessage
    )

    # Create the email message
    $Message = New-Object System.Net.Mail.MailMessage $notificationFromEmail, $notificationToEmail
    $Message.Subject = $subject
    $Message.Body = $emailMessage
    $Message.IsBodyHTML = $true

    # Set up the SMTP client
    $SMTPClient = New-Object Net.Mail.SmtpClient($SMTPServer, $SMTPPort)
    $SMTPClient.EnableSsl = $true
    $SMTPClient.Credentials = New-Object System.Net.NetworkCredential($notificationFromEmail, $notificationPassword)

    # Send the email
    $SMTPClient.Send($Message)
}

# Configure Exchange Session
Add-Type -Path $dllPath

$version = [Microsoft.Exchange.WebServices.Data.ExchangeVersion]::Exchange2010_SP2
$exchangeService = New-Object Microsoft.Exchange.WebServices.Data.ExchangeService($version)
$exchangeService.Url = New-Object Uri("https://mail.ethz.ch/EWS/Exchange.asmx")
$exchangeService.Credentials = New-Object System.Net.NetworkCredential -ArgumentList $email, $password

# Data for OOF-Settings
$oofSettings = New-Object Microsoft.Exchange.WebServices.Data.OofSettings
$oofSettings.State =  [Microsoft.Exchange.WebServices.Data.OofState]::Scheduled
$oofSettings.Duration = New-Object Microsoft.Exchange.WebServices.Data.TimeWindow($dateStart, $dateEnd)
$oofSettings.ExternalAudience = [Microsoft.Exchange.WebServices.Data.OofExternalAudience]::All
$oofSettings.InternalReply = New-Object Microsoft.Exchange.WebServices.Data.OofReply($messageIntern)
$oofSettings.ExternalReply = New-Object Microsoft.Exchange.WebServices.Data.OofReply($messageExtern)

try {
    # Set OOF settings
    $exchangeService.SetUserOofSettings($email, $oofSettings)

    # Log success message
    if ($enableLogging) {
        Log "OOF settings have been set for $email"
    }

    # Email success notification
    if ($enableNotification) {
        try {
            SendEmailNotification -Subject "Automated OOF-Reply has been successfully set" -emailMessage $emailMessage
        } catch {
            Log "Failed to send success email notification : $_"; 
            throw "Failed to success success email notification. Please check the log file in $logFilePath for more details."
        }
    }
} catch {
    # Email error notification
    if ($enableNotification) {
        try {
          SendEmailNotification -Subject "[WARNING] Failed to set OOF settings for $email" -Message "$_"
        } catch {
          Log "Failed to send error email notification : $_";
          throw "Failed to send error email notification. Please check the log file in $logFilePath for more details."}
    }

    # Log error message
    if ($enableLogging) {
        Log "Failed to set OOF settings for $email : $_"
        throw "Failed to set OOF settings for $email. Please check the log file in $logFilePath for more details."
    }
}