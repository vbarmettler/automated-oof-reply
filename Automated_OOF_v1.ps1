# Requires EWS Module, Microsoft.Exchange.WebServices.dll and ann.txt
# Credentials
$email = "vbarmettler@ethz.ch"
$filePath = Join-Path -Path $env:UserProfile -ChildPath "Documents\ann.txt"
$password = Get-Content $filepath | ConvertTo-SecureString

# Daten für die OOF-Replies
$current_date = Get-Date
$formatted_date = Get-Date -Format "yyyy-MM-dd"
$time_start = "00:00:00"
$time_end = "23:59:00"
$date_start = $formatted_date,$time_start -join "T"
$date_end = $formatted_date,$time_end -join "T"
$cultureEnglish = [System.Globalization.CultureInfo]::new("en-US")
$cultureSwiss = [System.Globalization.CultureInfo]::new("de-CH")
$return_date = $current_date.AddDays(1)

# Check if return date is a weekend and if thats the case set it to Monday
if ($return_date.DayOfWeek -eq "Saturday") {
    $return_date = $return_date.AddDays(2)
} 
elseif ($return_date.DayOfWeek -eq "Sunday") {
    $return_date = $return_date.AddDays(1)
}

# Oof Messages
$return_date_email_de = $return_date.ToString("dddd, dd. MMMM yyyy", $cultureSwiss)
$return_date_email_en = $return_date.ToString("dddd, dd. MMMM yyyy", $cultureEnglish)

$message_intern_template = Get-Content -Path "C:\Users\OOF\Documents\Message_Intern_OOF.html" -Raw
$message_intern = $message_intern_template -replace '\$return_date_email_de', $return_date_email_de -replace '\$return_date_email_en', $return_date_email_en

$message_extern_template = Get-Content -Path "C:\Users\OOF\Documents\Message_Extern_OOF.html" -Raw
$message_extern = $message_extern_template -replace '\$return_date_email_de', $return_date_email_de -replace '\$return_date_email_en', $return_date_email_en

# Configure Exchange Session
Add-Type -Path "C:\Users\OOF\Microsoft.Exchange.WebServices.dll"

$version = [Microsoft.Exchange.WebServices.Data.ExchangeVersion]::Exchange2010_SP2
$exchangeService = New-Object Microsoft.Exchange.WebServices.Data.ExchangeService($version)

$exchangeService.Url = New-Object Uri("https://mail.ethz.ch/EWS/Exchange.asmx")
$exchangeService.Credentials = New-Object System.Net.NetworkCredential -ArgumentList $email, $password

# Set Oof settings
$oofSettings = New-Object Microsoft.Exchange.WebServices.Data.OofSettings
$oofSettings.State =  [Microsoft.Exchange.WebServices.Data.OofState]::Scheduled
$oofSettings.Duration = New-Object Microsoft.Exchange.WebServices.Data.TimeWindow($date_start, $date_end)
$oofSettings.ExternalAudience = [Microsoft.Exchange.WebServices.Data.OofExternalAudience]::All
$oofSettings.InternalReply = New-Object Microsoft.Exchange.WebServices.Data.OofReply($message_intern)
$oofSettings.ExternalReply = New-Object Microsoft.Exchange.WebServices.Data.OofReply($message_extern)
$exchangeService.SetUserOofSettings($email, $oofSettings)
